import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TeamSalaryCalculatorTest {
    private static int DEFAULT_BASE_SALARY = 50000;
    private static double DEFAULT_HIGH_EXPERIENCE_COEF = 1.2;
    private static double DEFAULT_MEDIUM_EXPERIENCE_COEF = 1.1;
    private static int DEFAULT_HOURLY_WAGE = 3000;
    private TeamSalaryCalculator calculator = new TeamSalaryCalculator();

    private List<Employee> team;
    private double expected;
    private int base_salary;
    private double high_experience_coef;
    private double medium_experience_coef;
    private int hourly_wage;

    public TeamSalaryCalculatorTest(List<Employee> team, double expected, int base_salary, double high_experience_coef, double medium_experience_coef, int hourly_wage) {
        this.team = team;
        this.expected = expected;
        this.base_salary = base_salary;
        this.high_experience_coef = high_experience_coef;
        this.medium_experience_coef = medium_experience_coef;
        this.hourly_wage = hourly_wage;
    }

    @Parameterized.Parameters()
    public static Object[][] data() {
        return new Object[][]{
                {new ArrayList<>(Arrays.asList(
                        new Employee(0.75, 8, 1)
                )), 1.2 * 8 * DEFAULT_HOURLY_WAGE,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0, 8, 8)
                )), 0.9 * 8 * DEFAULT_HOURLY_WAGE * DEFAULT_HIGH_EXPERIENCE_COEF,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0.75, 8, 8),
                        new Employee(0.75, 8, 3)
                )), 1.2 * 8 * DEFAULT_HOURLY_WAGE * (DEFAULT_HIGH_EXPERIENCE_COEF + DEFAULT_MEDIUM_EXPERIENCE_COEF),
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0, 8, 8),
                        new Employee(0, 8, 8),
                        new Employee(0, 8, 8)
                )), 3 * 0.9 * 8 * DEFAULT_HOURLY_WAGE * DEFAULT_HIGH_EXPERIENCE_COEF * 1.75,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0.75, 8, 8),
                        new Employee(0.75, 8, 8),
                        new Employee(0.75, 8, 8)
                )), 3 * 1.2 * 8 * DEFAULT_HOURLY_WAGE * DEFAULT_HIGH_EXPERIENCE_COEF * 1.75,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0, 20, 1)
                )), 20 * DEFAULT_HOURLY_WAGE,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0.75, 3, 1),
                        new Employee(0.75, 3, 1),
                        new Employee(0.75, 3, 1),
                        new Employee(0.75, 3, 8),
                        new Employee(0.75, 3, 8),
                        new Employee(0.75, 3, 3),
                        new Employee(0.75, 3, 3)
                )), 1.2 * 3 * (2 * DEFAULT_HIGH_EXPERIENCE_COEF + 2 * DEFAULT_MEDIUM_EXPERIENCE_COEF + 3) * DEFAULT_HOURLY_WAGE * 1.75,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0.75, 3, 1),
                        new Employee(0.75, 3, 1),
                        new Employee(0.75, 3, 1),
                        new Employee(0.75, 3, 8),
                        new Employee(0.75, 3, 8),
                        new Employee(0.75, 3, 3),
                        new Employee(0.75, 3, 3),
                        new Employee(0.75, 3, 3)
                )), 1.2 * 3 * (2 * DEFAULT_HIGH_EXPERIENCE_COEF + 3 * DEFAULT_MEDIUM_EXPERIENCE_COEF + 3) * DEFAULT_HOURLY_WAGE,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0, 10, 1),
                        new Employee(0, 10, 1),
                        new Employee(0, 10, 1),
                        new Employee(0, 0, 8),
                        new Employee(0, 0, 8),
                        new Employee(0, 0, 3),
                        new Employee(0, 0, 3)
                )), 30,
                        30, 1, 1, 1},
                {new ArrayList<>(Arrays.asList(
                        new Employee(0, 10, 1),
                        new Employee(0, 10, 1)
                )), 20,
                        10, 1, 1, 1},
                {new ArrayList<>(), 0,
                        DEFAULT_BASE_SALARY, DEFAULT_HIGH_EXPERIENCE_COEF, DEFAULT_MEDIUM_EXPERIENCE_COEF, DEFAULT_HOURLY_WAGE},
        };
    }

    @Test
    public void test() {
        this.team.forEach(employee -> System.out.println(String.format(
                "Employee with %.2f innovation, %d presence time, and  %d year(s) of experience",
                employee.getInnovationCoef(),
                employee.getPresenceTime(),
                employee.getExperienceYears()))
        );
        System.out.println(String.format(
                "base_salary: %d, high_experience_coef: %.2f, medium_experience_coef: %.2f, hourly_wage: %d",
                this.base_salary,
                this.high_experience_coef,
                this.medium_experience_coef,
                this.hourly_wage
        ));
        System.out.println(String.format("Expected salary: %.3f", this.expected));

        assertEquals(
                this.expected,
                calculator.calculateTeamSalary(
                        this.team,
                        this.base_salary,
                        this.high_experience_coef,
                        this.medium_experience_coef,
                        this.hourly_wage
                ),
                0.001);
    }
}