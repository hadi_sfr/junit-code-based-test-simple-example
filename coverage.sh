#!/usr/bin/env sh

mvn test org.pitest:pitest-maven:mutationCoverage

myfiles=(target/pit-reports/*)
lastfile="${myfiles[${#myfiles[@]}-1]}"
unset myfiles
open $lastfile/index.html
